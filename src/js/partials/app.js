var app = angular.module('App', [
	'ngRoute',
	'ngTouch',
	'appControllers',
	'appServices'
]);

app.config(['$routeProvider', 
	function($routeProvider){
		$routeProvider.
			when('/articles', {
				controller: 'articlesController',
				templateUrl: 'partials/articles.htm'
			}).
			when('/articles/:articleId', {
				controller: 'articleController',
				templateUrl: 'partials/article.htm'
			}).
			otherwise({
				redirectTo: '/articles'
			});
	}
]);