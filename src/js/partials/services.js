var appServices = angular.module('appServices', ['ngResource']);

appServices.factory('Article', ['$resource',
	function($resource){
		return $resource('json/:articleId.json', {}, {
			query: {method:'GET', params:{articleId:'articles'}, isArray: true}
		});
	}
]);