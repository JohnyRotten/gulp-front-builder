var appControllers = angular.module('appControllers', []);

appControllers.controller('uiController', ['$scope', function($scope){
	$scope.menu = {
		opened: false
	};
	$scope.toggleMenu = function(){
		$scope.menu.opened = !$scope.menu.opened;
	};
}]);

appControllers.controller('articlesController', ['$scope', 'Article',
	function($scope, Article) {
		$scope.articles = Article.query();
	}
]);

appControllers.controller('articleController', ['$scope', '$routeParams', 'Article',
	function($scope, $routeParams, Article){
		$scope.article = Article.get({articleId: $routeParams.articleId},
			function(article){
				article.addComment = function(text){
					var d = new Date();
					var time = d.getDay() + '.' + d.getMonth() + '.' + d.getFullYear();
					article.comments.push({
						author: 'admin',
						text: text,
						time: time
					});
				};
			}
		);
	}
]);